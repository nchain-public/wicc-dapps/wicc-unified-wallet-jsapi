# waykiBridge第三方调用

## 引入方式
引入如下js
```
<script src="https://sw91.net/devops/js/waykiBridge.js"></script>
```

## 安装方式
npm install waykibridge

## 调用方式

参数：接口名，接口参数，成功回调，失败回调
```
waykiBridge.walletPlugin(name,query,function(res){},function(err){})
```
---

>注：query内属性均为string, 数组除外

>注：金额单位为 `sawi`

---

## API错误码说明

| errorCode (Int)             | errorMsg (String)                    | 含义                     |
| :------- | :------- | :------- |
| 0 | \ |操作成功|
| 1000 | Please install WaykiMax first. |没有安装钱包|
| 2000 | Please create or import wallet first. |没有创建或者导入钱包|
| 7000 | User denied transaction signature. |用户拒绝签名请求|

**注：** 开发者只需根据 ``errorCode``进行状态判断，`errorMsg` 只用于状态信息提示。

---

## getAddressInfo
query参数
```
{} //无参数请传空对象
```
example
```
    waykiBridge.walletPlugin(
        "getAddressInfo",
        {},
        function(res) {
            console.log(res)
        },
        function(err) {
            console.log(err)
        }
    );
```
成功回调
```
{ 
    "result": { 
        "account": { 
            "address": "WPqY8RJHN1u4Kzrnj2mHG9V8igJJVVcveb", 
            "id": "bd2356fa-6137-448e-806f-d6ab09785749", "testnetAddress": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa" 
            }, 
        "network": "testnet", 
        "address": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa" 
    }, 
    "errorCode": 0 
}
```

---

## walletPluginContractInvoke（合约调用）

query参数：

```
{
    "regId":"",     // 合约id
    "contractField", "", // 合约字段
    "inputAmount":"",   // 输入金额（没有传0）
    "remark" :""  // 备注，如：猜数字
}
```
example:
```
    waykiBridge.walletPlugin(
        "walletPluginTransfer",
        {
          regId: "13103-1",
          contractField: "f0",
          inputAmount: "1",
          remark : ""

        },
        function(res) {
            console.log(res)
        },
        function(err) {
            console.log(err)
        }
    );
```
成功回调
```
{ 
    "result": { 
        "amount": 100000000,
        "fee": 1000000, 
        "contract": "f0",
        "txid":"029c86a648030e2b28ccc64c5ed60c96a0c61de95a30cab82159a476ceeeaf3d", 
        "regId": "91647-1", 
        "txType": null,
        "toAddress": "wNTJYM3gyXJH9dPQe96ofyHotf5eq3EP14", 
        "sendAddress": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa" 
      }, 
    "errorCode": 0 
}
```
失败回调
```
{ "errorMsg": "errors.InvalidArgument is not a constructor" }
```


## walletPluginContractIssue（合约发布）
query参数
```
{
    "contractContent": "", //合约内容
    "contractDesc": ""     //合约描述
}
```
example
```
    waykiBridge.walletPlugin(
        "walletPluginContractInvoke",
        {
          contractContent: 'mylib = require "mylib"',
          contractDesc: "描述",
        },
        function(res) {
            console.log(res)
        },
        function(err) {
            console.log(err)
        }
    );
```
成功回调
```
{
    "result": { 
         "amount": 0, 
         "fee": 110000000, 
         "contract": null, 
         "txid": "4bbbee4320843d19d4a008bfc7dc1a26fdf5ed9c1a96415db033d4f83c6e9fa0", 
         "regId": "91647-1", 
         "txType": null,
         "toAddress": null,
         "sendAddress": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa"
    }, 
    "errorCode": 0 
}
```

## walletPluginTransfer
query参数
```
{
    "amount",""   //转账金额（单位wicc）
    "collectionAddress", "" //转账目标钱包的地址
    "remark": "" //备注
}
```
example:
```
    waykiBridge.walletPlugin(
        "walletPluginTransfer",
        {
          amount: "1", 
          collectionAddress: "Wi2H3XAhMtdLPkjQVSkYXhF3GRNTwAhtqA",
          remark: "备注"
        },
        function(res) {
          _this.transferRes = res;
        },
        function(err) {
          _this.transferRes = err;
        }
    );
```
成功回调
```
{ 
    "result": { 
        "amount": 10000000, 
        "fee": 10000, 
        "contract": "", 
        "txid": "64244292b9abb2e5b8d55a3f37584781e0151e0358a440b0e910100f9970957b",
        "regId": "91647-1", 
        "txType": null, 
        "toAddress": "weyg6FeZP5Mf6dNn1TQJbU7pHhLx1QMkZ8", 
        "sendAddress": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa" 
    }, 
    "errorCode": 0 
}
```

## walletPluginNodeVote
query参数
```
{
    "nodeList": [
    {
      "votedAddress":""  //被投票地址
      "voteCount": ""    //wicc数量
    }，
    ...
  ]
}
```
example
```
    waykiBridge.walletPlugin(
        "walletPluginNodeVote",
        {
          nodeList: [
              votedAddress: "Wi2H3XAhMtdLPkjQVSkYXhF3GRNTwAhtqA",
              voteCount: "1"
          ]
        },
        function(res) {
            console.log(res)
        },
        function(err) {
            console.log(err)
        }
    );
```
成功回调
```
{ 
    "result": {
         "amount": 0, 
         "fee": 10000, 
         "contract": null, 
         "txid": "9d9afcc62505ef256f54ca6eeeced369395698eb25487f89ea99b37d9be9479b", 
         "regId": 
         "91647-1", 
         "txType": null, 
         "toAddress": null, 
         "sendAddress": "wLnwB9n9PCdP2sWAF3R3KvMujxGnVsE6aa" 
    }, 
    "errorCode": 0 
    }
```


